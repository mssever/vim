" Custom filetypes
if exists("did_load_filetypes")
    finish
endif
augroup FileTypedetect
    "au! BufRead,BufNewFile *.odt                        setfiletype tar
    "au! BufRead,BufNewFile *.odp                        setfiletype zip
    "
    " Pandoc problems: It seems like I need a newer version of pandoc before
    " this'll work (>= 2.0). Alas, I can't get it for Ubuntu 18.04.
    "au! BufReadPost *.docx           :%!pandoc -s -f docx -t markdown
    "au! BufWritePost *.docx          :!pandoc -s -f markdown -t docx % > tmp.docx
    "au! BufReadPost *.odt            :%!pandoc -s -f odt -t markdown
    "au! BufWritePost *.odt           :!pandoc -s -f markdown -t odt % > tmp.odt
augroup END
