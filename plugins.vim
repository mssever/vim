"Setting this runtime path kills syntax highlighting.
"set runtimepath=~/.vim,/usr/share/vim/addons,/usr/share/vim/vimfiles,/usr/share/vim/vimcurrent,/usr/share/vim/vimfiles/after,/usr/share/vim/addons/after,~/.vim/after
filetype off                  " required

" set the runtime path to include Vundle and initialize
"if $VIM_MACHINE_ID ==# 'scott-ideapad-win.mss'
    "set rtp+='$HOME\\vimfiles\\bundle\\Vundle.vim'
    "set rtp+='C:/Users/Scott/vimfiles/bundle/Vundle.vim/'
"else
    set rtp+=~/.vim/bundle/Vundle.vim
"endif
"if $VIM_MACHINE_ID ==# 'scott-ideapad-win.mss'
   "exec 'source' g:vim_home.'bundle\Vundle.vim\autoload\vundle.vim'
   "call vundle#begin()
"else
call vundle#begin()
"endif
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
"... Auto-added
Plugin 'VundleVim/Vundle.vim'

if index(g:locally_installed_plugins, 'nerdtree') ==# -1
    Plugin 'scrooloose/nerdtree'
endif
exec 'source' g:vim_home.'plugin_config/plugin_nerdtree.vim'

if index(g:locally_installed_plugins, 'nerdtree-git-plugin') ==# -1
    Plugin 'Xuyuanp/nerdtree-git-plugin'
endif

if index(g:locally_installed_plugins, 'vim-gitgutter') ==# -1
    Plugin 'airblade/vim-gitgutter'
endif
exec 'source' g:vim_home.'plugin_config/plugin_gitgutter.vim'

if index(g:locally_installed_plugins, 'vim-indent-guides') ==# -1
    Plugin 'nathanaelkane/vim-indent-guides'
endif
exec 'source' g:vim_home.'plugin_config/plugin_vim-indent-guides.vim'

if index(g:locally_installed_plugins, 'airline') ==# -1
    Plugin 'vim-airline/vim-airline'
endif
exec 'source' g:vim_home.'plugin_config/plugin_airline.vim'

if index(g:locally_installed_plugins, 'airline-themes') ==# -1
    "Plugin URL:  https://github.com/vim-airline/vim-airline-themes
    Plugin 'vim-airline/vim-airline-themes'
endif

if index(g:locally_installed_plugins, 'ctrlp') ==# -1
    "Plugin URL: https://github.com/ctrlpvim/ctrlp.vim
    Plugin 'ctrlpvim/ctrlp.vim'
endif
exec 'source' g:vim_home.'plugin_config/plugin_ctrlp.vim'

if index(g:locally_installed_plugins, 'editorconfig') ==# -1
    "Plugin URL: https://github.com/editorconfig/editorconfig-vim
    Plugin 'editorconfig/editorconfig-vim'
endif

if index(g:locally_installed_plugins, 'fugitive') ==# -1
    "Plugin URL: https://github.com/tpope/vim-fugitive
    Plugin 'tpope/vim-fugitive'
endif

if index(g:locally_installed_plugins, 'lastplace') ==# -1
    "Plugin URL: https://github.com/farmergreg/vim-lastplace
    Plugin 'farmergreg/vim-lastplace'
endif

if index(g:locally_installed_plugins, 'youcompleteme') ==# -1
    "Plugin URL: https://github.com/Valloric/YouCompleteMe
    Plugin 'Valloric/YouCompleteMe'
endif
exec 'source' g:vim_home.'plugin_config/plugin_ycm.vim'

if index(g:locally_installed_plugins, 'polyglot') ==# -1
    "Plugin URL: https://github.com/sheerun/vim-polyglot
    Plugin 'sheerun/vim-polyglot'
endif
exec 'source' g:vim_home.'plugin_config/plugin_polyglot.vim'

if index(g:locally_installed_plugins, 'plasticboy-markdown') ==# -1
    "Plugin URL: https://github.com/plasticboy/vim-markdown
    Plugin 'godlygeek/tabular'
    Plugin 'plasticboy/vim-markdown'
endif
exec 'source' g:vim_home.'plugin_config/plugin_plasticboy-markdown.vim'

" Make spellcheck files sync smoothly via Git
if index(g:locally_installed_plugins, 'spellsync') ==# -1
    "Plugin URL: https://github.com/micarmst/vim-spellsync
    Plugin 'micarmst/vim-spellsync'
endif

" Show marks in the gutter
if index(g:locally_installed_plugins, 'gutter-marks') ==# -1
    "Plugin URL: https://github.com/kshenoy/vim-signature
    Plugin 'kshenoy/vim-signature'
endif
exec 'source' g:vim_home.'plugin_config/plugin_signature.vim'

" Multiple Cursors
" Too buggy: Runs slowly and has issues if you type a key while it's doing
" something else.
" if index(g:locally_installed_plugins, 'multiple-cursors') ==# -1
    " "Plugin URL: https://github.com/terryma/vim-multiple-cursors
    " Plugin 'terryma/vim-multiple-cursors'
" endif
" exec 'source' g:vim_home.'plugin_config/plugin_multiple_cursors.vim'

" Underline current word
if index(g:locally_installed_plugins, 'cursorword') ==# -1
    "Plugin URL: https://github.com/itchyny/vim-cursorword
    Plugin 'itchyny/vim-cursorword'
endif
exec 'source' g:vim_home.'plugin_config/plugin_cursorword.vim'

" JSON syntax highlighting which supports comments
if index(g:locally_installed_plugins, 'jsonc') ==# -1
    "Plugin URL: https://github.com/mssever/vim-json-comments
    Plugin 'mssever/vim-json-comments'
endif
exec 'source' g:vim_home.'plugin_config/plugin_jsonc.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

packadd! matchit
exec 'source' g:vim_home.'plugin_config/plugin_matchit.vim'

exec 'source' g:vim_home.'plugin_config/plugin_zip.vim'

" vim: ts=4 sw=4 et

