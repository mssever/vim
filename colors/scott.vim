runtime colors/industry.vim
let colors_name = "scott"
"Highlight types: term, cterm, gui
"Help topics: |highlight-args| |cterm-colors|
"Note guifg, guibg, ctermfg, ctermbg

"Base theme overrides
hi Constant                   ctermfg=cyan
hi Function     guifg=#33ff99 ctermfg=Green
hi Identifier   guifg=#ff3366 ctermfg=Magenta
hi Normal       guifg=#999999                   ctermbg=NONE
hi Special                    ctermfg=Red
hi Statement    guifg=#cccccc
hi Type         guifg=#33ff33 ctermfg=DarkGreen

" My colors
hi ColorColumn                guibg=#1c1c1c            ctermfg=black     ctermbg=darkgray
hi Comment      guifg=#cc6633               gui=italic ctermfg=darkgray                cterm=italic
hi CursorColumn guibg=#1c1c1c                                                          cterm=reverse term=reverse
hi Cursor       guifg=black   guibg=yellow  gui=bold   ctermfg=black     ctermbg=green
hi CursorLine   guibg=#1c1c1c                                                          cterm=reverse term=reverse
hi CursorLineNr guifg=#999900 guibg=#282819            ctermfg=white     ctermbg=black
hi NonText      guifg=#3333cc                          ctermfg=darkgray
hi FoldColumn   guifg=Cyan    guibg=#000033            ctermfg=8         ctermbg=11                  term=standout
hi Folded       guifg=#999999 guibg=#222222            ctermfg=11        ctermbg=8                   term=standout
hi GitGutterAddDefault guifg=#00ff00 guibg=black       ctermfg=Green     ctermbg=Black
hi GitGutterChangeDefault guifg=#ffff00 guibg=black    ctermfg=Yellow
hi GitGutterDeleteDefault     guibg=black              ctermfg=Red
hi iCursor      guifg=white   guibg=blue               ctermfg=white     ctermbg=blue
hi IndentGuidesOdd            guibg=#0f0f0f                              ctermbg=darkgray
hi IndentGuidesEven           guibg=#141414                              ctermbg=darkgray
hi LineNr       guifg=#666666 guibg=#111111            ctermfg=darkgray  ctermbg=black
hi MatchParen   guifg=yellow  guibg=#333333 gui=bold   ctermfg=Cyan      ctermbg=black cterm=bold,underline
hi Number       guifg=#cc22ff                          ctermfg=Magenta
hi Pmenu        guifg=White   guibg=#0019ff            ctermfg=white     ctermbg=blue
hi PmenuSel     guifg=#0019ff guibg=white              ctermfg=blue      ctermbg=white
hi Search       guifg=black   guibg=#666600            ctermfg=NONE      ctermbg=NONE  cterm=underline,bold,italic
hi SignColumn                 guibg=black                                ctermbg=black
hi StatusLine   guifg=#e4e4e4 guibg=#af2800 gui=bold
hi String       guifg=#0087af                          ctermfg=blue
hi VertSplit    guifg=#af2800 guibg=#111111 gui=bold   ctermfg=darkgreen ctermbg=black cterm=NONE
hi Visual       guifg=#003300 guibg=#00cc00            ctermfg=0         ctermbg=10                  term=reverse
hi WildMenu     guifg=#af2800 guibg=#e4e4e4 gui=bold
