let g:enable_powerline_symbols = 1

version 6.0
"if &cp | set nocp | endif   "What's the point of this?
let s:cpo_save=&cpo
set cpo&vim

let s:plug = ['ctrlp', 'fugitive', 'lastplace']
call extend(g:locally_installed_plugins, s:plug)

exec 'source' g:vim_home.'plugins.vim'
exec 'source' g:vim_home.'map.vim'

let &cpo=s:cpo_save
unlet s:cpo_save

exec 'source' g:vim_home.'settings.vim'
if !has('gui_running')
    " exec 'source' g:vim_home.'highlight.vim'
    colorscheme scott
endif
exec 'source' g:vim_home.'tag_autoviewer.vim'


" vim: ts=4 sw=4 et
