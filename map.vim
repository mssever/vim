let mapleader = "\\"
" let localleader = "\\"
command! WQ wq
command! Wq wq
command! W w
command! Q q
command! Qa qa
command! QA qa
" map! <xHome> <Home>
" map! <xEnd> <End>
" map! <S-xF4> <S-F4>
" map! <S-xF3> <S-F3>
" map! <S-xF2> <S-F2>
" map! <S-xF1> <S-F1>
" map! <xF4> <F4>
" map! <xF3> <F3>
" map! <xF2> <F2>
" map! <xF1> <F1>
" map <xHome> <Home>
" map <xEnd> <End>
" map <S-xF4> <S-F4>
" map <S-xF3> <S-F3>
" map <S-xF2> <S-F2>
" map <S-xF1> <S-F1>
" map <xF4> <F4>
" map <xF3> <F3>
" map <xF2> <F2>
" map <xF1> <F1>

" Get rid of ex mode
nnoremap Q <nop>

" Disable hlsearch
nnoremap <silent> <S-C-L> :nohlsearch<CR>


" Hit K to line-wrap a line (text wrap, textwrap)
"nnoremap K Vgqj
" nnoremap <silent> <C-K> /^.\{,30}$<CR>NjVnkgqn:nohlsearch<CR>
nnoremap <C-K> gqj
"nnoremap K /^.\{,30}$/e<CR>Nj

" Make / in a visual mode search for the selection
xnoremap / y/<C-R>"<CR>

" Quotes: Surround a word or selection with quotes
nnoremap <leader>' viw<esc>a'<esc>bi'<esc>lel
nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lel
xnoremap <leader>' <esc>`<i'<esc>`>la'<esc>
xnoremap <leader>" <esc>`<i"<esc>`>la"<esc>

"Brackets: Surround a word or selection with brackets
nnoremap <leader>< viw<esc>a><esc>bi<<esc>lel
nnoremap <leader>( viw<esc>a)<esc>bi(<esc>lel
nnoremap <leader>[ viw<esc>a]<esc>bi[<esc>lel
nnoremap <leader>{ viw<esc>a}<esc>bi{<esc>lel
xnoremap <leader>< <esc>`<i<<esc>`>la><esc>
xnoremap <leader>( <esc>`<i(<esc>`>la)<esc>
xnoremap <leader>[ <esc>`<i[<esc>`>la]<esc>
xnoremap <leader>{ <esc>`<i{<esc>`>la}<esc>

" Terminal: See https://stackoverflow.com/a/46822285/713735
if $VIM_MACHINE_ID ==# 'scott-ideapad-win.mss'
    nnoremap <silent> <leader>S :terminal ++curwin ++rows=10 ++close c:\Program Files\Git\bin\sh.exe --login<CR>
    nmap <silent> <leader>s :wincmd b<CR>\t:silent belowright split terminal<CR><C-W>J10<C-W>_\t:wincmd b<CR>:terminal ++curwin ++close c:\Program Files\Git\bin\sh.exe --login<CR>
    nnoremap <silent> <leader>p :terminal ++rows=15 ++close python -Bu<CR>
else
    nnoremap <silent> <leader>S :terminal ++rows=10 ++close<CR>
    nmap <silent> <leader>s :wincmd b<CR>\t:silent belowright split terminal<CR><C-W>J10<C-W>_\t:wincmd b<CR>:terminal ++curwin ++close<CR>
    nnoremap <silent> <leader>p :terminal ++rows=15 ++close bpython3<CR>
endif

"Terminal: Enable keys to escape to normal mode
tnoremap <S-PageUp> <C-W>N<PageUp>
tnoremap <F1> <C-W>N
" Resize terminal to normal size
tnoremap <F3> <C-W>10_
" Make terminal short
tnoremap <F2> <C-W>5_
" Make terminal tall
tnoremap <F4> <C-W>20_
" Make terminal really tall
tnoremap <F5> <C-W>40_

" Copying and pasting (may not work in terminal Vim)
noremap <S-C-C> "+y
noremap <S-C-X> "+x
noremap <S-C-V> "+gP

"Comments: <leader>c to comment and <leader>C to uncomment
" First, no-op the keys, then set them for specific filetypes.
nnoremap <leader>c <nop>
nnoremap <leader>C <nop>
xnoremap <leader>c <nop>
xnoremap <leader>C <nop>
augroup comments
    autocmd!
    " // comments
    au FileType javascript,scss,php   nn          <buffer> <leader>c ^i// <esc>
    au FileType javascript,scss,php   nn          <buffer> <leader>C 0:<C-U>s/\v^\s*\zs[\/]{2,} ?//<cr>:nohlsearch<cr>
    au FileType javascript,scss,php   xn <silent> <buffer> <leader>c :s/\v^\s*\zs/\/\/ /<CR>:nohlsearch<CR>
    au FileType javascript,scss,php   xn <silent> <buffer> <leader>C :s/\v^\s*\zs[\/]{2,} ?//<cr>:nohlsearch<CR>
    " # comments
    au FileType python,ruby,sh,yaml   nn          <buffer> <leader>c ^i# <esc>
    au FileType python,ruby,sh,yaml   nn          <buffer> <leader>C 0:<C-U>s/\v^\s*\zs#+ ?//<CR>:nohlsearch<CR>
    au FileType python,ruby,sh,yaml   xn <silent> <buffer> <leader>c :s/\v^\s*\zs/# /<CR>:nohlsearch<CR>
    au FileType python,ruby,sh,yaml   xn <silent> <buffer> <leader>C :s/\v^\s*\zs#+ ?//<CR>:nohlsearch<CR>
    " -- comments
    au FileType haskell,sql   nn          <buffer> <leader>c ^i-- <esc>
    au FileType haskell,sql   nn <silent> <buffer> <leader>C 0:<C-U>s/\v^\s*\zs--+ ?//<CR>:nohlsearch<CR>
    au FileType haskell,sql   xn <silent> <buffer> <leader>c :s /\v^\s*\zs/-- /<CR>:nohlsearch<CR>
    au FileType haskell,sql   xn <silent> <buffer> <leader>C :s/\v^\s*\zs--+ ?//<CR>:nohlsearch<CR>
    " <!-- --> comments
    au FileType html,markdown,xml,svg nn          <buffer> <leader>c ^i<!-- <esc>A --><esc>
    au FileType html,markdown,xml,svg nn <silent> <buffer> <leader>C 0:s/^\s*\zs<!-- \?//<CR>0/\m \?--><CR>df>:nohlsearch<CR>
    au FileType html,markdown,xml,svg xn          <buffer> <leader>c <esc>`>a --><esc>`<i<!-- <esc>`>
    au FileType html,markdown,xml,svg xn <silent> <buffer> <leader>C <esc>`<:'<,'>s/<!-- \?//<CR>:'<,'>s/ \?-->//<CR>:nohlsearch<CR>
    " /* */ comments
    au FileType css,json              nn <silent> <buffer> <leader>c ^i/* <esc>A */<esc>^:nohlsearch<CR>
    au FileType css,json              nn <silent> <buffer> <leader>C 0:s/\v^\s*\zs\/\*+ ?//<CR>0:s/\v ?\*+\///<CR>:nohlsearch<CR>
    au FileType css,json              xn <silent> <buffer> <leader>c <esc>`>a */<esc>`<i/* <esc>`>
    au FileType css,json              xn <silent> <buffer> <leader>C <esc>`<:'<,'>s/\v\/\*+ ?//<CR>`<:'<,'>s/\v ?\*+\///<CR>:nohlsearch<CR>`>
    " " comments
    au FileType vim                   nn          <buffer> <leader>c ^i" <esc>
    au FileType vim                   nn <silent> <buffer> <leader>C 0:<C-U>s/\v^\s*\zs["]+ ?//<CR>:nohlsearch<CR>
    au FileType vim                   xn <silent> <buffer> <leader>c :s/\v^\s*\zs/" /<CR>:nohlsearch<CR>
    au FileType vim                   xn <silent> <buffer> <leader>C :s/\v^\s*\zs["]+ ?//<CR>:nohlsearch<CR>
augroup END
"I'm forever hitting <leader>x instead of <leader>c. Make it a no-op.
nnoremap <leader>x <nop>
xnoremap <leader>x <nop>

" Make <space> a <PageDown> key in help files
augroup helpsettings
    autocmd!
    au FileType help nnoremap <buffer> <space> <PageDown>
    au FileType help nnoremap <buffer> <cr> <C-]>
augroup END

" Build/update tags in current working directory
nnoremap <leader>T :!ctags .<CR>

" Plugins:
if g:NERDTree_installed ==# 1
    nnoremap <leader>t :NERDTreeToggle<CR>
endif

" vim: ts=4 sw=4 et
