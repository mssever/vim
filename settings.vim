set autoindent
set backspace=indent,eol,start
set breakindent
set breakindentopt=shift:2
set colorcolumn+=80
set completeopt=menu,menuone " If preview is set, YCM will open a preview window with the function signature
set copyindent
set cryptmethod=blowfish2
set encoding=utf8
set expandtab
set fileencoding=utf8
set helplang=en
set history=200
set hlsearch
set incsearch
set laststatus=2
set linebreak
set modeline
set modelines=5
set mouse=a
" set mousefocus
set mousemodel=popup_setpos
set noequalalways
set number
set previewheight=6
set printoptions=paper:letter
set relativenumber
set ruler
set scrolloff=5
set shiftwidth=4
set showcmd
set showtabline=1
set sidescroll=1
set sidescrolloff=5
set smarttab
set suffixes=.bak,~,.swp,.o,.info,.aux,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out
set tabstop=4
set updatetime=250
set viminfo='100,<50,r/tmp,r/media,s10,h
set whichwrap=b,s,<,>,[,]
set wildmenu
set wildmode=longest:full,full

" Show certain white space characters
set list
if $VIM_MACHINE_ID ==# 'scott-ideapad-win.mss'
    "set listchars=tab:→—,trail:◡,eol:⏎
    set listchars=tab:→—,trail:◡
    "set listchars=tab:→—,space:·,trail:·,eol:⏎
    "set listchars=tab:>-,trail:~,eol:$
else
    "set listchars=tab:→—,trail:⋅,eol:⏎
    set listchars=tab:→—,trail:⋅
endif
set listchars+=precedes:<,extends:→

" The next three options enable Shift+arrows to enter select mode
set keymodel=startsel,stopsel
set selectmode=key
"set selection=exclusive

syntax enable

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal g'\"" | endif
endif

" Uncomment the following to have Vim load indentation rules according to the
" detected filetype. Per default Debian Vim only load filetype specific
" plugins.
if has("autocmd")
  filetype indent on
endif

augroup textsettings
    autocmd!
    au FileType markdown,html,text setlocal textwidth=80
    au FileType markdown,text      setlocal concealcursor=c
    au FileType markdown,text      setlocal conceallevel=2
    au FileType markdown,text,gitcommit,html setlocal spell spelllang=en_us
    " listchars actually can't be set local, so this change is global. Grr.
    "au FileType markdown,text      setlocal listchars-=space:⋅
    au FileType markdown,text      setlocal nolist
augroup END

augroup indentsettings
    autocmd!
    au FileType ruby,css,javascript,scss,php,html,haskell setlocal shiftwidth=2 tabstop=2
    " For some reason, the following settings break the mouse and cause gVim
    " to ignore all mouse input. I have no idea why...
    " au FileType ruby,css,javascript,scss,php,html let g:indent_guides_guide_size=2
    " au FileType python,markdown,html,text,gitcommit let g:indent_guides_guide_size=1
augroup END

augroup foldingsettings
    autocmd!
    au FileType ruby setlocal foldmethod=syntax foldnestmax=3
augroup END

" vim: ts=4 sw=4 et
