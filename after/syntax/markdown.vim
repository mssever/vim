syn match markdownIgnore "\v\w_\w"
syn match markdownIgnore "\v[a-zA-Z0-9]_[a-zA-Z0-9]"
syn match kramdownId "{[.:#][^}]*}"
syn cluster markdownInline add=kramdownId
highlight def link kramdownId Identifier
