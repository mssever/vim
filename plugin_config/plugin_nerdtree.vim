" Control NERDtree
"let loaded_nerd_tree = 1
"autocmd vimenter * NERDTree
let NERDTreeNaturalSort = 1
"let NERDTreeWinPos = "right"
"let NERDTreeWinSize = 26
let NERDTreeCreatePrefix = 'silent keepalt keepjumps'
let NERDTreeSortHiddenFirst=1
let NERDTreeShowHidden=1
let NERDTreeMouseMode=3
let NERDTreeHijackNetrw = 0
let NERDTreeChDirMode=2
let NERDTreeShowBookmarks=1
let NERDTreeMinimalUI=1
let g:NERDTree_installed=1
"let NERDTreeStatusline = %{exists('b:NERDTree')?b:NERDTree.root.path.str():''}
let NERDTreeIgnore=['\~$', '#$', '__pycache__', '\.pyc$', '\.swp$', '\.git$[[dir]]']
let NERDTreeSortOrder = []
call add(NERDTreeSortOrder, '\v^[^._].*\/$')
call add(NERDTreeSortOrder, '\v^[^_].*\.ya?ml$')
call add(NERDTreeSortOrder, '\v^[^_].*\.json$')
call add(NERDTreeSortOrder, '\.py$')
call add(NERDTreeSortOrder, '\.rb$')
call add(NERDTreeSortOrder, '\.php$')
call add(NERDTreeSortOrder, '\.js$')
call add(NERDTreeSortOrder, '\v\.(ba)?sh$')
call add(NERDTreeSortOrder, '\v\.(md|markdown|txt)$')
call add(NERDTreeSortOrder, '\v\.html?$')
call add(NERDTreeSortOrder, '\v\.s?css$')
call add(NERDTreeSortOrder, '*')
call add(NERDTreeSortOrder, '\v\.(gif|jpe?g|png|svg)$')
call add(NERDTreeSortOrder, '\v\.(gz|bz2|tgz|tbz2|Z|zip|svgz)$')
call add(NERDTreeSortOrder, '\.pdf$')
call add(NERDTreeSortOrder, '\v\.(avi|mov|mp[34]|ogg|midi?)$')
call add(NERDTreeSortOrder, '\v^[^.]*[^/]$')
call add(NERDTreeSortOrder, '\v^_.*\/$')
call add(NERDTreeSortOrder, '\M^_')
call add(NERDTreeSortOrder, '\v^\..*\/$')
call add(NERDTreeSortOrder, '^\.')
call add(NERDTreeSortOrder, '\.swp$')
call add(NERDTreeSortOrder, '\.bak$')
call add(NERDTreeSortOrder, '\~$')
