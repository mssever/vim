let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_toc_autofit = 1
let g:vim_markdown_conceal = 0
augroup vim_markdown
    autocmd!
    "au FileType markdown Toc
    au FileType markdown noremap <buffer> <leader>x :Toch<CR>
augroup END
