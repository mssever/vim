" Control the CtrlP plugin
let g:ctrlp_show_hidden = 1
let g:ctrlp_clear_cache_on_exit = 1
let g:ctrlp_custom_ignore = '\v[\/](\.git|\.hg|\.svn|_site)$'

