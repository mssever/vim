let g:polyglot_disabled=['markdown', 'json']
let g:python_highlight_all=1
let g:python_pep8_indent_hang_closing = 1
