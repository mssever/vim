" Control Airline
let g:airline_theme = 'dark'
"let g:airline_theme = 'badwolf'
"let g:airline_theme = 'base16_pop'
"let g:airline_theme = 'behelit'
" let g:airline_theme = 'cool'
"let g:airline_theme = 'luna'
let g:airline_inactive_collapse=1
let g:airline_skip_empty_sections=1
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'
" Airline will replace the mode display
set noshowmode

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" unicode symbols
"let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
"let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
"let g:airline_symbols.linenr = '␊'
"let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
"let g:airline_symbols.paste = 'Þ'
"let g:airline_symbols.paste = '∥'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

"Powerline symbols
if g:enable_powerline_symbols
  " let g:airline_left_sep = '' "▙▟▒
  let g:airline_left_sep = '▙'
  " let g:airline_left_sep = '▒'
  let g:airline_left_alt_sep = '║'
  " let g:airline_left_alt_sep = ''
  " let g:airline_right_sep = ''
  let g:airline_right_sep = '▟'
  " let g:airline_right_sep = '▒'
  let g:airline_right_alt_sep = '║'
  " let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
endif

let g:airline#extensions#wordcount#enabled = 1

