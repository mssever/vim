# How to install and use this repository

1. Clone and/or symlink this into your vim directory. On Linux, this is
   `~/.vim.` On Windows, it's `$HOME/vimfiles`. To make sure to also get the
   submodules, pass `--recurse` to `git clone`:

        git clone --recurse git@bitbucket.org:mssever/vim.git

2. On Windows, symlink `$HOME/.vim` to `$HOME/vimfiles`. Do to this, open
   `cmd.exe` as an administrator. _PowerShell won't work for this._ `cd` to your
   home directory (e.g., `C:\Users\Scott`). Then, use `mklink` to create the
   symlink:

        mklink /D .vim vimfiles

3. Now, create the environment variable `$VIM_MACHINE_ID` and set it to a unique
   name for your system, such as its hostname.

4. Next, create the startup script for your machine. It needs to be named
   `VIM_MACHINE_ID.vim`. The easiest way to create it is to copy an existing
   machine startup script and tweak it for your machine's environment. **NB**:
   The script `generic.vim` is what gets sourced if you don't configure
   `$VIM_MACHINE_ID` or create a machine-specific startup script.

5. If you need finer control, you can edit the other files and use statements
   like `if $VIM_MACHINE_ID ==# 'something'` when necessary, but try to keep
   your machine-specific code in the machine-specific startup file as much as
   possible.

6. Finally, re-launch vim and run `:PluginInstall` to install the plugins.
   Follow any instructions Vundle or its plugins give. In particular, note that
   youcompleteme isn't so simple to install. If you can't install it
   system-wide, you'll have to compile a part of it or disable it for your
   machine (To disable it, add `"youcompleteme"` to
   `g:locally_installed_plugins`, restart vim, and run `:PluginClean`.)

7. To make `Caps Lock` function as an escape key, [follow these
   instructions for Windows or Linux](https://stackoverflow.com/a/46516955/713735).

8. On Windows, if you get errors related to Python plugins, such as E887, make
   sure that only the correct Python DLL is available on the PATH. Open
   `cmd.exe` and run `where python*.dll`. If you see a DLL that isn't a part of
   the correct installation, remove it from your `PATH`. [Here's an
   explanation](https://vi.stackexchange.com/a/10012/20217).

