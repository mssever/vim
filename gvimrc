if !exists('g:vim_home')
    " Don't source this file when starting Gvim with --clean
    finish
endif
colorscheme scott
set guicursor+=n:blinkon0
set guicursor+=v:hor25-Cursor-blinkwait175-blinkoff150-blinkon175
if has('gui_win32')
    set guifont=Fira_Code_Light:h10:cANSI:qDRAFT
else
    " set guifont=Fira\ Code\ 12
    set guifont=Noto\ Mono\ 12
endif
set cursorline
"set guioptions=agimRLtk " Add T to display the toolbar
set guioptions+=acekt
set guioptions-=l
set guioptions-=L
set guioptions-=m
set guioptions-=r
set guioptions-=R
set guioptions-=T
"set lines=999 columns=999

" Fix Windows fonts. Modified from https://vi.stackexchange.com/a/6384/20217
if has('win32') || has('win64')
    set renderoptions=type:directx,level:0.75,gamma:1.25,contrast:0.25,geom:1,renmode:5,taamode:1
endif

if !has('win32') && !has('win64')
    augroup gvimrc
        autocmd! gvimrc
        autocmd GUIEnter * call system('wmctrl -i -b add,maximized_vert,maximized_horz -r '.v:windowid)
        "autocmd GUIEnter * exe "normal \<F5>"
    augroup END
endif
