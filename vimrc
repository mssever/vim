set nocompatible

if $VIM_MACHINE_ID ==# 'scott-ideapad-win.mss'
    "let gvim_home = get(g:, 'vim_home', 'C:/Users/Scott/vimfiles/') "expand('~/vimfiles/'))
    let g:vim_home = 'C:/Users/Scott/vimfiles/'
else
    let g:vim_home = get(g:, 'vim_home', expand('~/.vim/'))
endif

let g:locally_installed_plugins = []
let g:NERDTree_installed = 0

if !empty($VIM_MACHINE_ID)
    exec 'source' g:vim_home.$VIM_MACHINE_ID.'.vim'
else
    exec 'source' g:vim_home.'generic.vim'
endif
" VIM_MACHINE_ID='scott-laptop.mss'

" vim: ts=4 sw=4 et
